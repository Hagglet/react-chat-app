const users = []

function userJoin(id, username, room) {
  console.log(id)
    const user = {id, username, room}
    users.push(user)
    console.log("users", users)
    return user
}

function getCurrentUser(id) {
    return users.find(user => user.id === id)
}

// User leaves chat
function userLeave(id) {
  console.log(id)
    const index = users.findIndex(user => user.id === id);
    console.log(index)
  
    if (index !== -1) {
      return users.splice(index, 1)[0];
    }
  }
  
  // Get room users
  function getRoomUsers(room) {
      console.log(users.filter(user => user.room === room))
    return users.filter(user => user.room === room);
  }
  

  module.exports = {
    userJoin,
    getCurrentUser,
    userLeave,
    getRoomUsers
  };