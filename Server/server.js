
// All the requrie
var createError = require('http-errors');
var express = require('express');
const http = require('http')
const socketio = require('socket.io')
const fs = require('fs')
const jwt = require('jsonwebtoken');
const cors = require('cors')
const formatMessage = require('./public/utils/messages')
const privateKey = fs.readFileSync('./public/keys/private.pem', 'utf8')
const publicKey = fs.readFileSync('./public/keys/public.pem', "utf8")
const signOptions = { algorithm: 'RS256', expiresIn: '1m' };
const {
    userJoin,
    getCurrentUser,
    userLeave,
    getRoomUsers
} = require('./public/utils/usersFromRoom');

// use
var app = express();
app.use(cors())
const PORT = 4000 || process.env.PORT

const server = http.createServer(app)
//const io = socketio(server)
const botName = 'Time';


var io = require('socket.io')(server, {
    handlePreflightRequest: (req, res) => {
        const headers = {
            "Access-Control-Allow-Headers": "Content-Type, Authorization",
            "Access-Control-Allow-Origin": req.headers.origin, //or the specific origin you want to give access to,
            "Access-Control-Allow-Credentials": true
        };
        res.writeHead(200, headers);
        res.end();
    }
});


io.on("connection", (socket) => {

    if (socket.handshake.headers["authorization"]) {
        console.log("Authorization found: " + socket.handshake.headers["authorization"])
        const test = jwt.verify(socket.handshake.headers["authorization"], publicKey, { complete: true }, (error, authData) => {

            if (error) {
                console.log("disconneting")
                socket.emit('expired', 'disconnected')
                
            
            }
            else {
                console.log("valid token")

            }
        })
    }

    socket.on('joinRoom', ({ username, chat }) => {


        const user = userJoin(socket.id, username, chat)

        socket.join(user.room)

        socket.emit("message", formatMessage(botName, 'welcome'));

        // trying to sign token with JWT
        const token = jwt.sign({ "user": user, }, privateKey, { algorithm: 'RS256' })
        // emiting token
        socket.emit("token", token)


        socket.broadcast.to(user.room).emit('message', formatMessage(botName, `${user.username} has join the chat`))


        io.to(user.room).emit('Users', {
            room: user.room,
            usersFromRoom: getRoomUsers(user.room)
        });
    })

    socket.on('chatMessage', (message) => {
        const user = getCurrentUser(socket.id)
        io.to(user.room).emit('message', formatMessage(user.username, message));
    })

    socket.on('UserAndChat', (message) => {
        console.log(message);

    })
    socket.on('disconnect', () => {
        const user = userLeave(socket.id)
        if (user) {
            io.to(user.room).emit(
                'message',
                formatMessage(botName, `${user.username} has left the chat`)
            );
            io.to(user.room).emit('Users', {
                room: user.room,
                usersFromRoom: getRoomUsers(user.room)
            });
        }
    })


});

app.get("/getToken", (resp, res) => {
    const tokenToUser = resp.headers.username
    console.log(tokenToUser)
    const token = jwt.sign({ "user": tokenToUser, }, privateKey, signOptions)
    res.send({ token: token })
})






server.listen(PORT, () => console.log(`Server running on port ${PORT}`))